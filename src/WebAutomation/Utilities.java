package WebAutomation;
import WebAutomation.RegistrationForm;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;


public class Utilities {
	RegistrationForm rf;
	WebDriver driver = RegistrationForm.driver;
	WebElement fname = driver.findElement(By.id("fullname"));
	WebElement fnameText = driver.findElement(By.className("helpText"));
	WebElement fnameLabel = driver.findElement(By.className("formTitle"));
	WebElement email = driver.findElement(By.id("email"));
	WebElement emailHelpText = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[2]/div/div[2]"));
	WebElement mNumber = driver.findElement(By.id("mobnumber"));
	WebElement mobHelpText = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[3]/div/div[2]"));
	WebElement city = driver.findElement(By.id("city"));
	WebElement cityOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[4]/div/select/option[3]"));
    WebElement operation = driver.findElement(By.id("operation"));
    WebElement operationOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[5]/div/select/option[3]"));
    WebElement businessname = driver.findElement(By.id("businessname"));
    WebElement companyTurnover = driver.findElement(By.id("company_turnover"));
    WebElement turnoverOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[7]/div/select/option[4]"));
    WebElement sellOnline = driver.findElement(By.id("selling_online"));
    WebElement sellOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[9]/div/select/option[2]"));
    WebElement marketChkBox = driver.findElement(By.className("checkboxGroup"));
    WebElement myntra = driver.findElement(By.xpath("//input[@id='myntra']"));
    WebElement snapdeal = driver.findElement(By.xpath("//input[@id='snapdeal']"));
    WebElement amazon = driver.findElement(By.xpath("//input[@id='amazon']"));
    WebElement flipkart = driver.findElement(By.xpath("//input[@id='flipkart']"));
    WebElement ebay = driver.findElement(By.xpath("//input[@id='ebay']"));
    WebElement shopclues = driver.findElement(By.xpath("//input[@id='shopclues']"));
    WebElement paytm = driver.findElement(By.xpath("//input[@id='paytm']"));
    WebElement others = driver.findElement(By.xpath("//input[@id='others']"));
    
    
    
	String fullname = "fullname";
   // String email = "email";
    String mobile_number = "mobnumber";
   // String city = "city";
    String city_option = "/html/body/div[2]/div/div[1]/form/div[4]/div/select/option[3]";
    //String operation = "operation";
    String operation_option = "/html/body/div[2]/div/div[1]/form/div[5]/div/select/option[3]";
    //String businessname = "businessname";
    String company_turnover = "company_turnover";
    String company_turnover_option = "/html/body/div[2]/div/div[1]/form/div[7]/div/select/option[4]";
	//String sellOnline = "selling_online";
    //String sellOption = "/html/body/div[2]/div/div[1]/form/div[9]/div/select/option[2]";
    //String marketChkBox = "By.class(\"checkboxGroup\")";
    //String myntra = "myntra";
    //String snapdeal = "snapdeal";
    //String amazon = "amazon";
    //String flipkart = "flipkart";
    //String ebay = "ebay";   
    //String shopclues = "shopclues";
    //String paytm = "paytm";
    //String others = "others";
    
    public void scrollBrowser(WebDriver driver, String id){
    	JavascriptExecutor js = (JavascriptExecutor) driver;		
		js.executeScript("document.getElementById('id').scrollIntoView(true);");
    }
    
    public void fillFullName(String key){
    	fname.sendKeys(key);
		//driver.findElement(By.name(fullname)).sendKeys(key);
	}
    
    public void fillEmail(String key){
    	email.sendKeys(key);
    	//driver.findElement(By.name(email)).sendKeys(key);
    }
    
    public void fillMobNumber(String key){
    	mNumber.sendKeys(key);
    	//driver.findElement(By.name(mobile_number)).sendKeys(key);
    }
    
    public void chooseCity(WebDriver driver){
    	city.click();
    	cityOption.click();
    	//driver.findElement(By.name(city)).click();
    	//driver.findElement(By.xpath(city_option)).click();
    }
    
    public void chooseOperation(WebDriver driver, String key){
    	//driver.findElement(By.name(city)).click();
    	String opOption = "/html/body/div[2]/div/div[1]/form/div[5]/div/select/option[";
    	switch(key){
    	case "Private Limited":
    		opOption = opOption + "2]";
    		break;
    	case "Partnership":
    		opOption = opOption + "3]";
    		break;
    	case "Sole Proprietorship":
    		opOption = opOption + "4]";
    		break;
    	case "Limited Company (Unlisted)":
    		opOption = opOption + "5]";
    		break;
    	case "Other":
    		opOption = opOption + "6]";
    		break;	    						
    	}
    	driver.findElement(By.xpath(opOption)).click();
    }
    
    public void fillBusinessName(WebDriver driver, String key){
    	//driver.findElement(By.name(businessname)).sendKeys(key);
    	businessname.sendKeys(key);
    }
    
    /*
    <25        : 1
    25L - 50L  : 2
    50L - 1Cr  : 3
    1Cr - 2Cr  : 4
    2Cr - 3Cr  : 5
    3Cr - 4Cr  : 6
    4Cr - 5Cr  : 7
    5Cr - 6Cr  : 8
    6Cr - 7Cr  : 9
    7Cr - 8Cr  : 10
    8Cr - 9Cr  : 11
    9Cr - 10Cr : 12
    >10Cr      : 13
    */
    
    public void chooseCompanyTurnover(WebDriver driver, int key){
    	driver.findElement(By.name(company_turnover)).click();
    	String option = "/html/body/div[2]/div/div[1]/form/div[7]/div/select/option[";
    	switch(key){
    	case 1:
    		option = option + "2]";
    		break;
    	case 2:
    		option = option + "3]";
    		break;
    	case 3:
    		option = option + "4]";
    		break;
    	case 4:
    		option = option + "5]";
    		break;
    	case 5:
    		option = option + "6]";
    		break;
    	case 6:
    		option = option + "7]";
    		break;
    	case 7:
    		option = option + "8]";
    		break;
    	case 8:
    		option = option + "9]";
    		break;
    	case 9:
    		option = option + "10]";
    		break;
    	case 10:
    		option = option + "11]";
    		break;
    	case 11:
    		option = option + "12]";
    		break;
    	case 12:
    		option = option + "13]";
    		break;
    	case 13:
    		option = option + "14]";
    		break;			
    	}
    	driver.findElement(By.xpath(option)).click();
    }
    
    public void chooseRequiredLoan(WebDriver driver, int num){
    	WebElement we = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[8]/div/div[3]/div/span/span[1]"));
		Actions move = new Actions(driver);
		
		move.dragAndDropBy(we, 50, 0).build().perform();
    }
    
    public void chooseSellOnline(WebDriver driver, String key){
    	//driver.findElement(By.name(sellOnline)).click();
    	sellOnline.click();
    	String option = "/html/body/div[2]/div/div[1]/form/div[9]/div/select/option[";
    	switch(key){
    	case "yes":
    		option = option + "2]";
    		break;
    	case "no":
    		option = option + "3]";
    		break;
    	}
    	driver.findElement(By.xpath(option)).click();
    }
    
    //marketChkBox
    public void chooseMarketChkBox(WebDriver driver, String key ){
    	//scrollBrowser(driver, "myntra");
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	driver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);
		js.executeScript("document.getElementById('myntra').scrollIntoView(true);");
		driver.manage().timeouts().implicitlyWait(800, TimeUnit.SECONDS);
    		switch(key){
    		case "myntra":
    			driver.findElement(By.xpath("//input[@id='myntra']")).click();
    			break;
    		
    		case "snapdeal":
    			driver.findElement(By.xpath("//input[@id='snapdeal']")).click();
    			break;
    			
    		case "amazon":
    			driver.findElement(By.xpath("//input[@id='amazon']")).click();
    			break;
    			
    		case "flipkart":
    			driver.findElement(By.xpath("//input[@id='flipkart']")).click();
    			break;
    			
    		case "ebay":
    			driver.findElement(By.xpath("//input[@id='ebay']")).click();
    			break;
    			
    		case "shopclues":
    			driver.findElement(By.xpath("//input[@id='shopclues']")).click();
    			break;
    			
    		case "paytm":
    			driver.findElement(By.xpath("//input[@id='paytm']")).click();
    			break;
    			
    		case "others":
    			driver.findElement(By.xpath("//input[@id='others']")).click();
    			break;
    			
    		}	
    }
    
    public void setValues(WebDriver driver, String name, String email, 
    		String mobNum, String city, String operate, String company, 
    		int turnOver, int num, String sellonline){
    	fillFullName(name);
		fillEmail(email);
		fillMobNumber(mobNum);
		chooseCity(driver);
		chooseOperation(driver, operate);
		//ut.fillBusinessName(driver, "Company");
		chooseCompanyTurnover(driver, turnOver);		
		chooseRequiredLoan(driver, num);
		chooseSellOnline(driver, sellonline);
		
		//driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
		
		//chooseMarketChkBox(driver, "myntra");
    }
    
	public String helpName(){
		return fnameText.getText();
	}
	
	public String helpEmail(){
		return emailHelpText.getText();	
	}
	
	public String helpMobNumber(){
		return mobHelpText.getText();
	}
}
